# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  # Redefining vim via vim_configurable enables automatic syntax support for .nix files
  # TODO(akavel): also understand `vim_configurable.merge` mentioned in:
  # http://lists.science.uu.nl/pipermail/nix-dev/2016-March/019872.html
  vim = pkgs.vim_configurable.override {
    name = "vim";
    vimrcConfig = {};
  };

  l4linux = pkgs.callPackage ./l4linux.nix {};
  # TODO(akavel): [LATER] newer nixpkgs have 1-arg linuxPackagesFor
  l4linuxPackages = pkgs.linuxPackagesFor l4linux l4linuxPackages;

in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  # boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  # Hyper-V
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  boot.initrd.checkJournalingFS = false;  # not sure if needed; was suggested for VirtualBox
  boot.initrd.kernelModules = ["hv_vmbus" "hv_storvsc"];  # https://github.com/nixos/nixpkgs/issues/9899
  #boot.kernelParams = ["vga=795"];
  boot.kernelParams = ["video=hyperv_fb:800x600"]; # https://askubuntu.com/a/399960
  boot.kernel.sysctl."vm.overcommit_memory" = "1"; # avoid https://github.com/NixOS/nix/issues/421

  # Hyper-V: client for shared folder on Windows Hyper-V host
  # Source: nixpkgs.git/nixos/tests/samba.nix
  fileSystems."/vm-share" = {
    fsType = "cifs";
    device = "//10.0.0.100/shared-space";
    options = [ "username=shares-guest" "password=zfHy9sxrXnJE7dEtcQgg" ];
  };
  networking.interfaces.eth1.ip4 = [{address="10.0.0.101"; prefixLength=28;}];

  # Try enabling L4Linux kernel
  boot.kernelPackages = l4linuxPackages;
  #boot.kernelPackages = pkgs.linuxPackagesFor (pkgs.linux);

  services.nixosManual.showManual = true; # Ctrl-Alt-F8

  # networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    vim
    git
  #  wget
    zerofree  # tool to free up ext2/3/4 blocks for reclaiming/compacting by VM host
    psmisc    # `fuser` - tool for checking what processes are still using a mount point
  ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.kdm.enable = true;
  # services.xserver.desktopManager.kde4.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.extraUsers.guest = {
  #   isNormalUser = true;
  #   uid = 1000;
  # };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "16.09";

  # Keep each NixOS build's source in /run/current-system/configuration.nix
  system.copySystemConfiguration = true;
}
